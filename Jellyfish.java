
public class Jellyfish {

	private String colour;
	private String shape;
	private boolean swimmer;
	
	
	public void identifyJellyfish(){
		
		System.out.println("Identifying your jellyfish...");
		
		if (colour.equals("transparent") && 
			shape.equals("saucer") && !swimmer) {
				
			System.out.println("You've found a Moon Jellyfish!");
				
		} else if (colour.equals("blue") && 
			shape.equals("box") && swimmer) {
			
			System.out.println("Be careful, that's a Box Jellyfish!");
		
		} else {
			
			System.out.println("Jellyfish Indentity Unknown.");
		}
	}
	
	public void checkVenom(){
		
		if (!swimmer) {
			System.out.println("It has a bit of venom, bit it'll only irritate your skin.");
			
		} else {
			System.out.println("Its sting is super lethal!");
		
		}
	}
	
	public Jellyfish(String color, String shape, boolean swimmer){
		this.colour = color;
		this.shape = shape;
		this.swimmer = swimmer;
	}
	
		
			// -- Getter/Setter methods
	
	public void setColor(String newColor) {
		this.colour = newColor;
	}
	
	public void setShape(String newShape) {
		this.colour = newShape;
	}
	
	public void setSwimmer(boolean isSwimmer) {
		this.swimmer = isSwimmer;
	}
	
	
	public String getColor() {
		return this.colour;
	}
	
	public String getShape() {
		return this.shape;
	}
		
	public boolean getSwimmer() {
		return this.swimmer;
	}
}
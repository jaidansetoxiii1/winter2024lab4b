import java.util.Scanner;


public class VirtualPetApp {
	
	public static void main (String [] args){
		Scanner reader = new Scanner(System.in);
		
		//System.out.println("");
		
		Jellyfish[] bloom = new Jellyfish[1];
		
		System.out.println("Jellyfish Identifier");
		System.out.println("Please answer the following questions");
		
		
		for (int i=0; i<bloom.length; i++){
			
			// colour question
			System.out.println("What colour is this jellyfish?");
			String color = reader.nextLine();
			
			// shape question
			System.out.println("What shape is it?");
			String shape = reader.nextLine();
			
			
			//swimmer question
			System.out.println("Can it swim on its own? Y/N");
			String answer = reader.next();
			boolean swimmer = false;
			
			if(answer.equals("y") || answer.equals("Y")){
				swimmer = true;
			
			} else {
				swimmer = false;
			}
			
			
			
			bloom[i] = new Jellyfish(color, shape, swimmer);
			
			
		}
		
		System.out.println("Features of the last animal: Colour: " + bloom[bloom.length - 1].getColor() + " Shape: " + bloom[bloom.length - 1].getShape() + " Swimmer: " + bloom[bloom.length - 1].getSwimmer());
		
		System.out.println("Hey, this last Jellyfish is shiny! What color is it?");
		bloom[bloom.length - 1].setColor(reader.next());
		
		System.out.println("Features of the last animal: Colour: " + bloom[bloom.length - 1].getColor() + " Shape: " + bloom[bloom.length - 1].getShape() + " Swimmer: " + bloom[bloom.length - 1].getSwimmer());
		
	}


}